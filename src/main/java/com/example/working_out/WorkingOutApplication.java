package com.example.working_out;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkingOutApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkingOutApplication.class, args);

    }

}
